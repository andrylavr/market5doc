# Market5
Технология Market5 позволяет создавать Desktop приложения, используя современные скриптовые языки и инструменты, в частности:
* HTML5, JS, CSS
* NodeJS, NPM
* PHP
* Ruby
* Python
* Redis
и др.
## Создание простого приложения

Создайте папку для будущего проекта:
```bash
mkdir FirstApp
cd FirstApp
```

Инициализируйте приложение:
```bash
npm init
```

